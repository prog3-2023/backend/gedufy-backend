package curso.output;

import curso.model.Curso;

public interface CrearCursoRepository {
    Integer guardarCurso(Curso curso);
}
