package persona.input;

import persona.exception.PersonaYaExisteException;
import persona.model.Persona;
import persona.usecase.crearpersonausecase.CrearPersonaRequestModel;

public interface CrearPersonaInput {
    Integer crearPersona(CrearPersonaRequestModel crearPersonaRequestModel) throws PersonaYaExisteException;
}
