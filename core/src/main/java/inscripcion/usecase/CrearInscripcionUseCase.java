package inscripcion.usecase;

import inscripcion.input.CrearInscripcionInput;
import inscripcion.model.Inscripcion;
import inscripcion.output.CrearInscripcionRepository;

public class CrearInscripcionUseCase implements CrearInscripcionInput {

    private CrearInscripcionRepository crearInscripcionRepository;
    public CrearInscripcionUseCase(CrearInscripcionRepository crearInscripcionRepository) {
        this.crearInscripcionRepository = crearInscripcionRepository;
    }

    @Override
    public Integer crearInscripcion(Inscripcion personaCurso) {
        return crearInscripcionRepository.guardarInscripcion(personaCurso);
    }
}
