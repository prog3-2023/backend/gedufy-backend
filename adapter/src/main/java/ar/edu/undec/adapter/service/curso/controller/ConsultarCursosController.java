package ar.edu.undec.adapter.service.curso.controller;

import curso.input.ConsultarCursosInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("cursos")
@CrossOrigin(origins = "*")
public class ConsultarCursosController {

    private ConsultarCursosInput consultarCursosInput;

    @Autowired
    public ConsultarCursosController(ConsultarCursosInput consultarCursosInput) {
        this.consultarCursosInput = consultarCursosInput;
    }

    @GetMapping
    public ResponseEntity<?> consultarCursos() {
        try {
            return ResponseEntity.ok(consultarCursosInput.obtenerCursos());
        } catch (Exception e) {
            return ResponseEntity.noContent().build();
        }
    }
}
