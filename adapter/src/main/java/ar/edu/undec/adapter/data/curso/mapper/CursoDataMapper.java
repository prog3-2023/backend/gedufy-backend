package ar.edu.undec.adapter.data.curso.mapper;

import ar.edu.undec.adapter.data.categoria.mapper.CategoriaDataMapper;
import ar.edu.undec.adapter.data.curso.exception.CursoMapeoException;
import ar.edu.undec.adapter.data.curso.model.CursoEntity;
import curso.model.Curso;

public class CursoDataMapper {

    public static Curso dataCoreMapper(CursoEntity cursoEntity) {
        try {
            return Curso.instancia(cursoEntity.getId(),
                    cursoEntity.getNombre(),
                    cursoEntity.getDescripcion(),
                    cursoEntity.getUrl(),
                    cursoEntity.getImagen(),
                    cursoEntity.getCantHoras(),
                    cursoEntity.getPrecio(),
                    CategoriaDataMapper.dataCoreMapper((cursoEntity.getCategoria())));
        } catch (Exception e) {
            throw new CursoMapeoException("Error al mapear Curso de Entidad a Core");
        }
    }

    public static CursoEntity dataEntityMapper(Curso curso) {
        try {
            return new CursoEntity(curso.getId(),
                    curso.getNombre(),
                    curso.getDescripcion(),
                    curso.getUrl(),
                    curso.getImagen(),
                    curso.getCantHoras(),
                    curso.getPrecio(),
                    CategoriaDataMapper.dataEntityMapper(curso.getCategoria()));
        } catch (Exception e) {
            throw new CursoMapeoException("Error al mapear Curso de Core a Entidad");
        }
    }
}
