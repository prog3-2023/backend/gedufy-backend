package curso.usecase;

import curso.input.CrearCursoInput;
import curso.model.Curso;
import curso.output.CrearCursoRepository;

public class CrearCursoUseCase implements CrearCursoInput {
    private CrearCursoRepository crearCursoRepository;

    public CrearCursoUseCase(CrearCursoRepository crearCursoRepository) {
        this.crearCursoRepository = crearCursoRepository;
    }

    @Override
    public Integer crearCurso(Curso curso) {
        return crearCursoRepository.guardarCurso(curso);
    }
}
