package ar.edu.undec.adapter.service.persona.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import persona.output.CrearPersonaRepository;
import persona.usecase.crearpersonausecase.CrearPersonaUseCase;
import tipopersona.usecase.ConsultarTipoPersonaPorIdUseCase;

@Configuration
public class PersonaBeanConfig {

    @Bean
    public CrearPersonaUseCase crearPersonaUseCase(CrearPersonaRepository crearPersonaRepository, ConsultarTipoPersonaPorIdUseCase consultarTipoPersonaPorIdUseCase) {
        return new CrearPersonaUseCase(crearPersonaRepository, consultarTipoPersonaPorIdUseCase);
    }
}
