package modeltest;

import categoria.model.Categoria;
import curso.exception.CursoIncompletoException;
import curso.model.Curso;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CursoUnitTest {

    @Test
    public void instanciar_atributosCorrectos_Instancia() {
        // Arrange
        Categoria categoria = Categoria.instancia(1, "Programación", "descripcion");

        // Act
        Curso curso = Curso.instancia(1, "Angular para principiante", "descripcion", "url", ".../img.png", 80, 2500.0, categoria);

        // Assert
        Assertions.assertNotNull(curso);
    }

    @Test
    public void instanciar_faltaNombre_CursoIncompletoException() {
        Categoria categoria = Categoria.instancia(1, "Programación", "descripcion");

        Exception exceptionVacio = Assertions.assertThrows(CursoIncompletoException.class, () -> Curso.instancia(1, "", "descripcion", "url", ".../img.png", 80, 2500.0, categoria));
        Exception exceptionNulo = Assertions.assertThrows(CursoIncompletoException.class, () -> Curso.instancia(1, null, "descripcion", "url", ".../img.png", 80, 2500.0, categoria));
        Assertions.assertEquals("Falta nombre", exceptionVacio.getMessage());
        Assertions.assertEquals("Falta nombre", exceptionNulo.getMessage());
    }

    @Test
    public void instanciar_faltaImagen_CursoIncompletoException() {
        Categoria categoria = Categoria.instancia(1, "Programación", "descripcion");

        Exception exceptionVacio = Assertions.assertThrows(CursoIncompletoException.class, () -> Curso.instancia(1, "Angular para principiante", "descripcion", "url", "", 80, 2500.0, categoria));
        Exception exceptionNulo = Assertions.assertThrows(CursoIncompletoException.class, () -> Curso.instancia(1, "Angular para principiante", "descripcion", "url", null, 80, 2500.0, categoria));
        Assertions.assertEquals("Falta imagen", exceptionVacio.getMessage());
        Assertions.assertEquals("Falta imagen", exceptionNulo.getMessage());
    }

    @Test
    public void instanciar_faltaPrecio_CursoIncompletoException() {
        Categoria categoria = Categoria.instancia(1, "Programación", "descripcion");

        Exception exceptionNulo = Assertions.assertThrows(CursoIncompletoException.class, () -> Curso.instancia(1, "Angular para principiante", "descripcion", "url", ".../img.png", 80, null, categoria));
        Assertions.assertEquals("Falta precio", exceptionNulo.getMessage());
    }

    @Test
    public void instanciar_faltaCategoria_CursoIncompletoException() {
        Exception exceptionNulo = Assertions.assertThrows(CursoIncompletoException.class, () -> Curso.instancia(1, "Angular para principiante", "descripcion", "url", ".../img.png", 80, 2500.0, null));
        Assertions.assertEquals("Falta categoria", exceptionNulo.getMessage());
    }
}
