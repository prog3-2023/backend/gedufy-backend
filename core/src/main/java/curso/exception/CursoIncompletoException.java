package curso.exception;

public class CursoIncompletoException extends RuntimeException{
    public CursoIncompletoException(String message) {
        super(message);
    }
}
