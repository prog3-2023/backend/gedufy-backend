package usecase;

import curso.model.Curso;
import factory.InscripcionFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import inscripcion.input.ConsultarInscripcionesInput;
import inscripcion.output.ConsultarInscripcionesRepository;
import inscripcion.usecase.ConsultarInscripcionesUseCase;

import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ConsultarInscripcionesUseCaseUnitTest {

    ConsultarInscripcionesInput consultarInscripcionesInput;

    @Mock
    ConsultarInscripcionesRepository consultarInscripcionesRepository;

    @BeforeEach
    void setup() {
        consultarInscripcionesInput = new ConsultarInscripcionesUseCase(consultarInscripcionesRepository);
    }

    @Test
    void consultarInscripciones_ExistenDatos_DevuelveLista() {
        when(consultarInscripcionesRepository.obtenerInscripciones(1)).thenReturn(InscripcionFactory.listaDeCursosDePersonaID1(3));
        List<Curso> resultado = consultarInscripcionesInput.obtenerInscripciones(1);
        Assertions.assertEquals(3, resultado.size());
        Assertions.assertNotNull(resultado);
    }

    @Test
    void consultarInscripciones_NoExistenDatos_DevuelveListaVacia() {
        when(consultarInscripcionesRepository.obtenerInscripciones(1)).thenReturn(Collections.emptyList());
        List<Curso> resultado = consultarInscripcionesInput.obtenerInscripciones(1);
        Assertions.assertEquals(0, resultado.size());
        Assertions.assertNotNull(resultado);
    }
}
