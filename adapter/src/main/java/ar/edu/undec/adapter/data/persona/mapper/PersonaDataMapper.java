package ar.edu.undec.adapter.data.persona.mapper;

import ar.edu.undec.adapter.data.persona.exception.PersonaMapeoException;
import ar.edu.undec.adapter.data.persona.model.PersonaEntity;
import ar.edu.undec.adapter.data.tipopersona.mapper.TipoPersonaDataMapper;
import persona.model.Persona;

public class PersonaDataMapper {

    public static Persona dataCoreMapper(PersonaEntity personaEntity) {
        try {
            return Persona.instancia(personaEntity.getId(),
                    personaEntity.getDni(),
                    personaEntity.getNombre(),
                    personaEntity.getEmail(),
                    personaEntity.getTelefono(),
                    personaEntity.getFechaNacimiento(),
                    TipoPersonaDataMapper.dataCoreMapper((personaEntity.getTipoPersona())));
        } catch (Exception e) {
            throw new PersonaMapeoException("Error al mapear Persona de Entidad a Core");
        }
    }

    public static PersonaEntity dataEntityMapper(Persona persona) {
        try{
        return new PersonaEntity(persona.getId(),
                persona.getDni(),
                persona.getNombre(),
                persona.getEmail(),
                persona.getTelefono(),
                persona.getFechaNacimiento(),
                TipoPersonaDataMapper.dataEntityMapper(persona.getTipoPersona()));
        } catch (Exception e) {
            throw new PersonaMapeoException("Error al mapear Persona de Core a Entidad");
        }
    }
}
