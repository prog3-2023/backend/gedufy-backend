package ar.edu.undec.adapter.service.curso.config;

import curso.output.ConsultarCursosRepositorio;
import curso.usecase.consultarcursosusecase.ConsultarCursosUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CursoBeanConfig {

    @Bean
    public ConsultarCursosUseCase consultarCursosUseCase(ConsultarCursosRepositorio consultarCursosRepositorio) {
        return new ConsultarCursosUseCase(consultarCursosRepositorio);
    }
}
