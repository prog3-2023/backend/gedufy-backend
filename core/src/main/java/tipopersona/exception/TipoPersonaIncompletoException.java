package tipopersona.exception;

public class TipoPersonaIncompletoException extends RuntimeException {
    public TipoPersonaIncompletoException(String message) {
        super(message);
    }
}
