package inscripcion.model;

import curso.model.Curso;
import persona.model.Persona;
import inscripcion.exception.InscripcionIncompletaException;

public class Inscripcion {
    private Integer id;
    private Persona persona;
    private Curso curso;
    private String observaciones;

    private Inscripcion(Integer id, Persona persona, Curso curso, String observaciones) {
        this.id = id;
        this.persona = persona;
        this.curso = curso;
        this.observaciones = observaciones;
    }

    public static Inscripcion instancia(Integer id, Persona persona, Curso curso, String observaciones) {
        if (persona == null) {
            throw new InscripcionIncompletaException("Falta persona");
        }
        if (curso == null) {
            throw new InscripcionIncompletaException("Falta curso");
        }
        return new Inscripcion(id, persona, curso, observaciones);
    }

    public Curso getCurso() {
        return curso;
    }

    public Persona getPersona() {
        return persona;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }
}
