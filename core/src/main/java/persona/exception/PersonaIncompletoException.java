package persona.exception;

public class PersonaIncompletoException extends RuntimeException {
    public PersonaIncompletoException(String message) {
        super(message);
    }
}
