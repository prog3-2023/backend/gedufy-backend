package usecase;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import persona.exception.PersonaNoExisteException;
import persona.output.VerificarPersonaRepository;
import persona.usecase.VerificarPersonaPorEmailUseCase;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class VerificarPersonaPorEmailUseCaseUnitTest {
    VerificarPersonaPorEmailUseCase verificarPersonaPorEmailUseCase;
    @Mock
    VerificarPersonaRepository verificarPersonaRepository;

    @BeforeEach
    void setup() {
        verificarPersonaPorEmailUseCase = new VerificarPersonaPorEmailUseCase(verificarPersonaRepository);
    }

    @Test
    void verificarPersonaPorEmail_PersonaExiste_RetornarEmail() {
        // Arrange
        String email = "mail@mail.com";
        when(verificarPersonaRepository.existePersonaPorEmail(email)).thenReturn(true);

        // Act
        String resultado = verificarPersonaPorEmailUseCase.verificarExistePersonaPorEmail(email);

        // Assert
        Assertions.assertNotNull(resultado);
        Assertions.assertEquals("mail@mail.com", resultado);
    }

    @Test
    void verificarPersonaPorEmail_PersonaNoExiste_PersonaNoExisteException() {
        // Arrange
        String email = "mail@mail.com";
        when(verificarPersonaRepository.existePersonaPorEmail(email)).thenReturn(false);

        // Act
        Exception exceptionEmailNoExiste = Assertions.assertThrows(PersonaNoExisteException.class, () -> verificarPersonaPorEmailUseCase.verificarExistePersonaPorEmail(email));

        // Assert
        Assertions.assertEquals("No existe persona con email mail@mail.com", exceptionEmailNoExiste.getMessage());
    }
}
