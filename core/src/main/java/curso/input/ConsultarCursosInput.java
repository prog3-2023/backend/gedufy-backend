package curso.input;

import curso.usecase.consultarcursosusecase.ConsultarCursosResponseModel;

import java.util.List;

public interface ConsultarCursosInput {
    List<ConsultarCursosResponseModel> obtenerCursos();
}
