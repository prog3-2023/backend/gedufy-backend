package curso.usecase.consultarcursosusecase;

import curso.input.ConsultarCursosInput;
import curso.output.ConsultarCursosRepositorio;

import java.util.List;
import java.util.stream.Collectors;

public class ConsultarCursosUseCase implements ConsultarCursosInput {
    private ConsultarCursosRepositorio consultarCursosRepositorio;

    public ConsultarCursosUseCase(ConsultarCursosRepositorio consultarCursosRepositorio) {
        this.consultarCursosRepositorio = consultarCursosRepositorio;
    }

    @Override
    public List<ConsultarCursosResponseModel> obtenerCursos() {
        List<ConsultarCursosResponseModel> consultarCursosResponseModelList =
                consultarCursosRepositorio.obtenerCursos().stream().map(curso -> ConsultarCursosResponseModel.instancia(
                        curso.getId(),
                        curso.getNombre(),
                        curso.getUrl(),
                        curso.getImagen(),
                        curso.getCantHoras(),
                        curso.getPrecio(),
                        curso.getCategoria().getNombre())).collect(Collectors.toList());

        return consultarCursosResponseModelList;
    }
}
