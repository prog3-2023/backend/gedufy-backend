package persona.input;

public interface IngresarInput {
    String verificarExistePersonaPorEmail(String email);
}
