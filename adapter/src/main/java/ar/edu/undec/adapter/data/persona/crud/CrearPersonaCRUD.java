package ar.edu.undec.adapter.data.persona.crud;

import ar.edu.undec.adapter.data.persona.model.PersonaEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;

@Repository
public interface CrearPersonaCRUD extends CrudRepository<PersonaEntity, Integer> {

    Boolean existsByDni(String dni);

    Boolean existsByEmail(String email);
}
