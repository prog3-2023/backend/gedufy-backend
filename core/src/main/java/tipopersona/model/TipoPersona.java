package tipopersona.model;

import tipopersona.exception.TipoPersonaIncompletoException;

public class TipoPersona {

    private Integer id;
    private String nombre;
    private String descripcion;

    private TipoPersona(Integer id, String nombre, String descripcion) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
    }

    public static TipoPersona instancia(Integer id, String nombre, String descripcion) {
        if (nombre == null || nombre.isEmpty()) {
            throw new TipoPersonaIncompletoException("Falta nombre");
        }
        return new TipoPersona(id, nombre, descripcion);
    }

    public Integer getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }
}
