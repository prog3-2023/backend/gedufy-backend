package curso.input;

import curso.model.Curso;

public interface CrearCursoInput {
    Integer crearCurso(Curso curso);
}
