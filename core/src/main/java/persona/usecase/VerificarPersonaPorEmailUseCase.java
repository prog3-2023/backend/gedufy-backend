package persona.usecase;

import persona.exception.PersonaNoExisteException;
import persona.input.IngresarInput;
import persona.output.VerificarPersonaRepository;

public class VerificarPersonaPorEmailUseCase implements IngresarInput {

    private VerificarPersonaRepository verificarPersonaRepository;

    public VerificarPersonaPorEmailUseCase(VerificarPersonaRepository verificarPersonaRepository) {
        this.verificarPersonaRepository = verificarPersonaRepository;
    }

    @Override
    public String verificarExistePersonaPorEmail(String email) {
        if (!verificarPersonaRepository.existePersonaPorEmail(email)) {
            throw new PersonaNoExisteException(String.format("No existe persona con email %s", email));
        }
        return email;
    }
}
