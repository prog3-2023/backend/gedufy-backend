package ar.edu.undec.adapter.service.tipopersona.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import tipopersona.output.ConsultarTipoPersonaPorIdRepository;
import tipopersona.usecase.ConsultarTipoPersonaPorIdUseCase;

@Configuration
public class TipoPersonaBeanConfig {

    @Bean
    public ConsultarTipoPersonaPorIdUseCase consultarTipoPersonaPorIdUseCase(ConsultarTipoPersonaPorIdRepository consultarTipoPersonaPorIdRepository) {
        return new ConsultarTipoPersonaPorIdUseCase(consultarTipoPersonaPorIdRepository);
    }
}
