# gedufy-backend

## Nombre

gedufy-backend

## Descripcion

Proyecto integrador de la materia Programación 3 - 2022.

## Stack utilizado

Maven, Java, Spring Boot.

## Arquitectura

Clean Architecture - Principios SOLID - Patrones de Diseño

## Buenas prácticas y conceptos a considerar

- La nomenclatura de paquetes será en minúsculas
- La nomenclatura de clases será en UpperCamelCase
- La nomenclatura de métodos será en lowerCamelCase
- La organización de paquetes será por modelo->aspecto, tanto a nivel src/main como a nivel src/test. Ejemplo:
  ```
  computadora
  └─ excepciones
  └─ modelo
  └─ repositorio
  └─ casodeuso
  ```
- Se deben crear pruebas unitarias tanto a nivel Core como a nivel Adapter
- Usar Excepciones personalizadas
- Se debe usar método factory/instancia para crear objetos
- Nomenclatura representativa de clases, métodos, etc.

## DER Conceptual

![alt text](<./der-gedufy.png>)

## Restricciones

- No puede existir más de una Persona con el mismo dni.
- No puede existir más de una Persona con el mismo email.
- Atributo obligatorio de TipoPersona: nombre.
- Atributos obligatorios de Persona: nombre, email, TipoPersona.
- Atributo obligatorio de Categoria: nombre.
- Atributos obligatorios de Curso: nombre, imagen, precio, Categoria.
- Atributos obligatorios de Inscripcion: Persona, Curso.

## Casos de uso

- Registrar un Curso.
- Consultar la lista de Cursos.
- Registrar una Persona.
- Ingresar con una Persona existente.  
- Comprar un curso (Registrar una Inscripcion).
- Consultar la lista de Cursos de una Persona (Inscripciones).

## Funcionalidades

- Persona (Registrar)
    - Endpoint [POST]: http://localhost:8080/personas
    - RequestBody:
  ```json
    {
      "id": null,
      "dni": "30222555",    
      "nombre": "Facundo Campazo",
      "email": "facu@mail.com",
      "telefono": null,
      "fechaNacimiento": null,
      "tipoPersonaId": 1
    }
    ```

- Persona (Ingresar)
    - Endpoint [GET]: http://localhost:8080/personas/sign-in
    - RequestBody:
  ```json
    {
      "email": "facu@mail.com"
    }
    ```


- Curso (Consultar)
    - Endpoint [GET]: http://localhost:8080/cursos


- Inscripcion (Registrar)
    - Endpoint [POST]: http://localhost:8080/inscripciones
    - RequestBody:
  ```json
    {
      "id": null,
      "personaId": 1,
      "cursoId": 1,
      "observaciones": "Pago de contado..."
    }
    ```

- Inscripcion (Consultar)
  - Endpoint [GET]: http://localhost:8080/inscripciones/by-persona
  - RequestBody:
  ```json
    {
      "email": "facu@mail.com"
    }
    ```
