package usecase;

import curso.input.ConsultarCursosInput;
import curso.output.ConsultarCursosRepositorio;
import curso.usecase.consultarcursosusecase.ConsultarCursosResponseModel;
import curso.usecase.consultarcursosusecase.ConsultarCursosUseCase;
import factory.CursoFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ConsultarCursosUseCaseUnitTest {

    ConsultarCursosInput consultarCursosInput;

    @Mock
    ConsultarCursosRepositorio consultarCursosRepositorio;

    @BeforeEach
    void setup() {
        consultarCursosInput = new ConsultarCursosUseCase(consultarCursosRepositorio);
    }

    @Test
    void consultarCursos_ExistenCursos_DevuelveLista() {
        when(consultarCursosRepositorio.obtenerCursos()).thenReturn(CursoFactory.listaDeCursos(3));
        List<ConsultarCursosResponseModel> resultado = consultarCursosInput.obtenerCursos();
        Assertions.assertEquals(3, resultado.size());
        Assertions.assertNotNull(resultado);
    }

    @Test
    void consultarCursos_NoExistenCursos_DevuelveListaVacia() {
        when(consultarCursosRepositorio.obtenerCursos()).thenReturn(Collections.emptyList());
        List<ConsultarCursosResponseModel> resultado = consultarCursosInput.obtenerCursos();
        Assertions.assertEquals(0, resultado.size());
        Assertions.assertNotNull(resultado);
    }
}
