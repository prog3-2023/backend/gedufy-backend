package inscripcion.output;

import inscripcion.model.Inscripcion;

import java.util.Collection;

public interface ConsultarInscripcionesRepository {
    Collection<Inscripcion> obtenerInscripciones(Integer personaId);
}
