package inscripcion.input;

import inscripcion.model.Inscripcion;

public interface CrearInscripcionInput {
    Integer crearInscripcion(Inscripcion personaCurso);
}
