package ar.edu.undec.adapter.data.persona.exception;

public class PersonaMapeoException extends RuntimeException {
    public PersonaMapeoException(String message) {
        super(message);
    }
}
