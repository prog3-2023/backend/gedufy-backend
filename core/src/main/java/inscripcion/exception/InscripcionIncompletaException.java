package inscripcion.exception;

public class InscripcionIncompletaException extends RuntimeException {
    public InscripcionIncompletaException(String message) {
        super(message);
    }
}
