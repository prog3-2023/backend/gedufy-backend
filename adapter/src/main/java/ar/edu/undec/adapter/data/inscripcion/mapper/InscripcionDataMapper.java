package ar.edu.undec.adapter.data.inscripcion.mapper;

import ar.edu.undec.adapter.data.curso.mapper.CursoDataMapper;
import ar.edu.undec.adapter.data.inscripcion.exception.InscripcionMapeoException;
import ar.edu.undec.adapter.data.inscripcion.model.InscripcionEntity;
import ar.edu.undec.adapter.data.persona.mapper.PersonaDataMapper;
import inscripcion.model.Inscripcion;

public class InscripcionDataMapper {

    public static Inscripcion dataCoreMapper(InscripcionEntity inscripcionEntity) {
        try {
            return Inscripcion.instancia(inscripcionEntity.getId(),
                    PersonaDataMapper.dataCoreMapper((inscripcionEntity.getPersona())),
                    CursoDataMapper.dataCoreMapper((inscripcionEntity.getCurso())),
                    inscripcionEntity.getObservaciones());
        } catch (Exception e) {
            throw new InscripcionMapeoException("Error al mapear Inscripcion de Entidad a Core");
        }
    }

    public static InscripcionEntity dataEntityMapper(Inscripcion inscripcion) {
        try {
            return new InscripcionEntity(inscripcion.getId(),
                    PersonaDataMapper.dataEntityMapper(inscripcion.getPersona()),
                    CursoDataMapper.dataEntityMapper(inscripcion.getCurso()),
                    inscripcion.getObservaciones());
        } catch (Exception e) {
            throw new InscripcionMapeoException("Error al mapear Inscripcion de Core a Entidad");
        }
    }
}
