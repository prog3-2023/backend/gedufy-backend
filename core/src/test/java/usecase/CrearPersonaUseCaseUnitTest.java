package usecase;

import factory.PersonaFactory;
import factory.TipoPersonaFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import persona.exception.PersonaYaExisteException;
import persona.model.Persona;
import persona.output.CrearPersonaRepository;
import persona.usecase.crearpersonausecase.CrearPersonaRequestModel;
import persona.usecase.crearpersonausecase.CrearPersonaUseCase;
import tipopersona.input.ConsultarTipoPersonaPorIdInput;
import tipopersona.usecase.ConsultarTipoPersonaPorIdUseCase;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CrearPersonaUseCaseUnitTest {
    CrearPersonaUseCase crearPersonaUseCase;
    @Mock
    CrearPersonaRepository crearPersonaRepository;
    @Mock
    ConsultarTipoPersonaPorIdUseCase consultarTipoPersonaPorIdUseCase;

    @BeforeEach
    void setup() {
        crearPersonaUseCase = new CrearPersonaUseCase(crearPersonaRepository, consultarTipoPersonaPorIdUseCase);
    }

    @Test
    void crearPersona_PersonaNoExiste_CrearPersona() {
        CrearPersonaRequestModel crearPersonaRequestModel = PersonaFactory.factoryCrearPersonaRequestModel(1);

        when(consultarTipoPersonaPorIdUseCase.consultarTipoPersonaPorId(1)).thenReturn(TipoPersonaFactory.factoryTipoPersona(1));
        when(crearPersonaRepository.existePorDNI("27444555")).thenReturn(false);
        when(crearPersonaRepository.existePorEmail("manu@mail.com")).thenReturn(false);
        when(crearPersonaRepository.guardarPersona(any(Persona.class))).thenReturn(1);

        Assertions.assertEquals(1, crearPersonaUseCase.crearPersona(crearPersonaRequestModel));
    }

    @Test
    void crearPersona_PersonaExistePorDni_PersonaYaExisteException() {
        CrearPersonaRequestModel crearPersonaRequestModel = PersonaFactory.factoryCrearPersonaRequestModel(1);

        when(crearPersonaRepository.existePorDNI("27444555")).thenReturn(true);
        verify(crearPersonaRepository, never()).guardarPersona(any(Persona.class));

        Exception exceptionDni = Assertions.assertThrows(PersonaYaExisteException.class, () -> crearPersonaUseCase.crearPersona(crearPersonaRequestModel));
        Assertions.assertEquals("Ya existe persona con dni 27444555", exceptionDni.getMessage());
    }

    @Test
    void crearPersona_PersonaExistePorEmail_PersonaYaExisteException() {
        CrearPersonaRequestModel crearPersonaRequestModel = PersonaFactory.factoryCrearPersonaRequestModel(1);

        when(crearPersonaRepository.existePorEmail("manu@mail.com")).thenReturn(true);
        verify(crearPersonaRepository, never()).guardarPersona(any(Persona.class));

        Exception exceptionEmail = Assertions.assertThrows(PersonaYaExisteException.class, () -> crearPersonaUseCase.crearPersona(crearPersonaRequestModel));
        Assertions.assertEquals("Ya existe persona con email manu@mail.com", exceptionEmail.getMessage());
    }
}
