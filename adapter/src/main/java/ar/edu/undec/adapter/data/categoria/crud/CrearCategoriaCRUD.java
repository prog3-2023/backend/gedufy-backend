package ar.edu.undec.adapter.data.categoria.crud;

import ar.edu.undec.adapter.data.categoria.model.CategoriaEntity;
import org.springframework.data.repository.CrudRepository;

public interface CrearCategoriaCRUD extends CrudRepository<CategoriaEntity, Integer> {
}
