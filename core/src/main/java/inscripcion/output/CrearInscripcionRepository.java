package inscripcion.output;

import inscripcion.model.Inscripcion;

public interface CrearInscripcionRepository {
    Integer guardarInscripcion(Inscripcion personaCurso);
}
