package ar.edu.undec.adapter.data.tipopersona.crud;

import ar.edu.undec.adapter.data.tipopersona.model.TipoPersonaEntity;
import org.springframework.data.repository.CrudRepository;

public interface CrearTipoPersonaCRUD extends CrudRepository<TipoPersonaEntity, Integer> {
}
