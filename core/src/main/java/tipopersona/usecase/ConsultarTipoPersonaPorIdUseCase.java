package tipopersona.usecase;

import tipopersona.input.ConsultarTipoPersonaPorIdInput;
import tipopersona.model.TipoPersona;
import tipopersona.output.ConsultarTipoPersonaPorIdRepository;

public class ConsultarTipoPersonaPorIdUseCase implements ConsultarTipoPersonaPorIdInput {

    private ConsultarTipoPersonaPorIdRepository consultarTipoPersonaPorIdRepository;

    public ConsultarTipoPersonaPorIdUseCase(ConsultarTipoPersonaPorIdRepository consultarTipoPersonaPorIdRepository) {
        this.consultarTipoPersonaPorIdRepository = consultarTipoPersonaPorIdRepository;
    }

    @Override
    public TipoPersona consultarTipoPersonaPorId(Integer tipoPersonaId) {
        return consultarTipoPersonaPorIdRepository.consultarTipoPersonaPorId(tipoPersonaId);
    }

    /*
    ConsultarInscripcionPersonaResponseModel

    Integer idInscripcion
    String nombrePersona
    String dni
    String email
    String cursoNombre
    String url
    String categoriaCurso
    Integer idCurso
    Integer idPersona

    List<Inscripcion> inscripciones= FindInscripcionPorDniPersona()
    inscripciones.stream.map(
        laInsc=>{
            ConsultarInscripcionPersonaResponseModel.instancia(
                laInsc.getId(),
                laInsc.getPersona().getNombre(),
                laInsc.getPersona().getDni(),
                laInsc.getPersona().getEmail(),
                laInsc.getCurso().getNombre(),
                laInsc.getCurso().getUrl(),
                laInsc.getCurso().getCategoria().getNombre(),
                laInsc.getCurso().getId(),
                laInsc.getPersona().getId())})
        .collect(Collectors.toList();

    private Integer id;
    private Persona persona;
    private Curso curso;
    private String observaciones;

    Curso
    private Integer id;
    private String nombre;
    private String descripcion;
    private String url;
    private String imagen;
    private Integer cantHoras;
    private Double precio;
    private Categoria categoria;

    Persona
    private Integer id;
    private String dni;
    private String nombre;
    private String email;
    private String telefono;
    private LocalDate fechaNacimiento;
    private TipoPersona tipoPersona;
     */
}
