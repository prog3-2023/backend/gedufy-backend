package persona.model;

import persona.exception.PersonaIncompletoException;
import tipopersona.model.TipoPersona;
import utils.EmailValidator;
import utils.exception.InvalidEmailException;

import java.time.LocalDate;

public class Persona {

    private Integer id;
    private String dni;
    private String nombre;
    private String email;
    private String telefono;
    private LocalDate fechaNacimiento;
    private TipoPersona tipoPersona;

    private Persona(Integer id, String dni, String nombre, String email, String telefono, LocalDate fechaNacimiento, TipoPersona tipoPersona) {
        this.id = id;
        this.dni = dni;
        this.nombre = nombre;
        this.email = email;
        this.telefono = telefono;
        this.fechaNacimiento = fechaNacimiento;
        this.tipoPersona = tipoPersona;
    }

    public static Persona instancia(Integer id, String dni, String nombre, String email, String telefono, LocalDate fechaNacimiento, TipoPersona tipoPersona) {
        if (nombre == null || nombre.isEmpty()) {
            throw new PersonaIncompletoException("Falta nombre");
        }
        if (email == null || email.isEmpty()) {
            throw new PersonaIncompletoException("Falta email");
        }
        if (tipoPersona == null) {
            throw new PersonaIncompletoException("Falta tipo de persona");
        }
        if (!EmailValidator.isValid(email)) {
            throw new InvalidEmailException("Email inválido");
        }
        return new Persona(id, dni, nombre, email, telefono, fechaNacimiento, tipoPersona);
    }

    public String getDni() {
        return dni;
    }

    public String getEmail() {
        return email;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public TipoPersona getTipoPersona() {
        return tipoPersona;
    }
}
