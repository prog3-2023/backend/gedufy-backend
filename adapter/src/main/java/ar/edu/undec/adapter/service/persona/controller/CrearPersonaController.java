package ar.edu.undec.adapter.service.persona.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import persona.exception.PersonaYaExisteException;
import persona.input.CrearPersonaInput;
import persona.usecase.crearpersonausecase.CrearPersonaRequestModel;

@RestController
@RequestMapping("personas")
public class CrearPersonaController {

    private CrearPersonaInput crearPersonaInput;

    @Autowired
    public CrearPersonaController(CrearPersonaInput crearPersonaInput) {
        this.crearPersonaInput = crearPersonaInput;
    }

    @PostMapping
    public ResponseEntity<?> crearPersona(@RequestBody CrearPersonaRequestModel crearPersonaRequestModel) {
        try {
            return ResponseEntity.created(null).body(crearPersonaInput.crearPersona(crearPersonaRequestModel));
        } catch (PersonaYaExisteException e) {
           return ResponseEntity.badRequest().build();
        }
    }
}
