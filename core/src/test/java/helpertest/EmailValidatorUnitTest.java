package helpertest;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import utils.EmailValidator;

public class EmailValidatorUnitTest {

    @Test
    void isValid_validEmail_returnsTrue() {
        String[] validAddress = {"me@example.com",
                "a.nonymous@example.com",
                "name+tag@example.com",
                "email@subdomain.example.com",
                "firstname+lastname@example.com",
                "email@123.123.123.123",
                "1234567890@example.com",
                "email@example-one.com",
                "_______@example.com",
                "email@example.name",
                "email@example.museum",
                "email@example.co.jp",
                "firstname-lastname@example.com"};
        for (String email : validAddress) {
            Assertions.assertTrue(EmailValidator.isValid(email));
        }
    }

    @Test
    void isValid_invalidEmail_returnsFalse() {
        String[] invalidAddress = {
                "me@",
                "@example.com",
                "me.@example.com",
                ".me@example.com",
                "me@example..com",
                "me.example@com",
                "me\\@example.com",
                "name\\@tag@example.com",
                "spaces\\ are\\ allowed@example.com",
                "\"spaces may be quoted\"@example.com",
                "!#$%&\"*+-/=.?^_`{|}~@[1.0.0.127]",
                "!#$%&\"*+-/=.?^_`{|}~@[IPv6:0123:4567:89AB:CDEF:0123:4567:89AB:CDEF]",
                "me(this is a comment)@example.com",
                "email@[123.123.123.123]",
                "\"email\"@example.com"
        };
        for (String email : invalidAddress) {
            Assertions.assertFalse(EmailValidator.isValid(email));
        }
    }
}
