package modeltest;

import categoria.model.Categoria;
import curso.model.Curso;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import persona.model.Persona;
import inscripcion.exception.InscripcionIncompletaException;
import inscripcion.model.Inscripcion;
import tipopersona.model.TipoPersona;

import java.time.LocalDate;

public class InscripcionUnitTest {

    @Test
    public void instanciar_atributosCorrectos_Instancia() {
        // Arrange
        TipoPersona tipoPersona = TipoPersona.instancia(1, "Alumno", "descripcion");
        Persona persona = Persona.instancia(1, "27444555", "Manu Ginobili", "manu@mail.com", "+543516112233", LocalDate.of(1980, 1, 20), tipoPersona);
        Categoria categoria = Categoria.instancia(1, "Programación", "descripcion");
        Curso curso = Curso.instancia(1, "Angular para principiante", "descripcion", "url", ".../img.png", 80, 2500.0, categoria);

        // Act
        Inscripcion personaCurso = Inscripcion.instancia(1, persona, curso, "observaciones");

        // Assert
        Assertions.assertNotNull(personaCurso);
    }

    @Test
    public void instanciar_faltaPersona_InscripcionIncompletoException() {
        Categoria categoria = Categoria.instancia(1, "Programación", "descripcion");
        Curso curso = Curso.instancia(1, "Angular para principiante", "descripcion", "url", ".../img.png", 80, 2500.0, categoria);
        Exception exceptionNulo = Assertions.assertThrows(InscripcionIncompletaException.class, () -> Inscripcion.instancia(1, null, curso, "observaciones"));
        Assertions.assertEquals("Falta persona", exceptionNulo.getMessage());
    }

    @Test
    public void instanciar_faltaCurso_InscripcionIncompletoException() {
        TipoPersona tipoPersona = TipoPersona.instancia(1, "Alumno", "descripcion");
        Persona persona = Persona.instancia(1, "27444555", "Manu Ginobili", "manu@mail.com", "+543516112233", LocalDate.of(1980, 1, 20), tipoPersona);
        Exception exceptionNulo = Assertions.assertThrows(InscripcionIncompletaException.class, () -> Inscripcion.instancia(1, persona, null, "observaciones"));
        Assertions.assertEquals("Falta curso", exceptionNulo.getMessage());
    }
}
