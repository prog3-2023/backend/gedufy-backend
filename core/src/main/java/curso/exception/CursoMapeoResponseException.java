package curso.exception;

public class CursoMapeoResponseException extends RuntimeException {
    public CursoMapeoResponseException(String message) {
        super(message);
    }
}
