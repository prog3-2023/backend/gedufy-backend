package ar.edu.undec.adapter.data.curso.crud;

import ar.edu.undec.adapter.data.curso.model.CursoEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;

public interface ConsultarCursosCRUD extends CrudRepository<CursoEntity, Integer> {
    Collection<CursoEntity> findAll();
}
