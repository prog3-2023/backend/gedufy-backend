package usecase;

import factory.InscripcionFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import inscripcion.input.CrearInscripcionInput;
import inscripcion.model.Inscripcion;
import inscripcion.output.CrearInscripcionRepository;
import inscripcion.usecase.CrearInscripcionUseCase;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CrearInscripcionUseCaseUnitTest {
    CrearInscripcionInput crearInscripcionInput;
    @Mock
    CrearInscripcionRepository crearInscripcionRepository;

    @BeforeEach
    void setup() {
        crearInscripcionInput = new CrearInscripcionUseCase(crearInscripcionRepository);
    }

    @Test
    void crearInscripcion_InscripcionNueva_CreaInscripcion() {
        Inscripcion inscripcion = InscripcionFactory.factoryInscripcion(1);
        when(crearInscripcionRepository.guardarInscripcion(inscripcion)).thenReturn(1);
        Assertions.assertEquals(1, crearInscripcionInput.crearInscripcion(inscripcion));
    }
}
