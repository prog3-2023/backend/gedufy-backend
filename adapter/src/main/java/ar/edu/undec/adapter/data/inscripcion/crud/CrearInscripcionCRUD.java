package ar.edu.undec.adapter.data.inscripcion.crud;

import ar.edu.undec.adapter.data.inscripcion.model.InscripcionEntity;
import org.springframework.data.repository.CrudRepository;

public interface CrearInscripcionCRUD extends CrudRepository<InscripcionEntity, Integer> {
}
