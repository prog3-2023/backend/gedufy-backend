package inscripcion.input;

import curso.model.Curso;

import java.util.List;

public interface ConsultarInscripcionesInput {
    List<Curso> obtenerInscripciones(Integer personaId);
}
