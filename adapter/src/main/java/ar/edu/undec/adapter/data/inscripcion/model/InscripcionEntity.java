package ar.edu.undec.adapter.data.inscripcion.model;

import ar.edu.undec.adapter.data.curso.model.CursoEntity;
import ar.edu.undec.adapter.data.persona.model.PersonaEntity;

import javax.persistence.*;
@Entity(name = "inscripcion")
public class InscripcionEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @JoinColumn(name = "persona_id", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private PersonaEntity persona;
    @JoinColumn(name = "curso_id", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private CursoEntity curso;
    private String observaciones;

    public InscripcionEntity() {
    }

    public InscripcionEntity(Integer id, PersonaEntity persona, CursoEntity curso, String observaciones) {
        this.id = id;
        this.persona = persona;
        this.curso = curso;
        this.observaciones = observaciones;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public PersonaEntity getPersona() {
        return persona;
    }

    public void setPersona(PersonaEntity persona) {
        this.persona = persona;
    }

    public CursoEntity getCurso() {
        return curso;
    }

    public void setCurso(CursoEntity curso) {
        this.curso = curso;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }
}
