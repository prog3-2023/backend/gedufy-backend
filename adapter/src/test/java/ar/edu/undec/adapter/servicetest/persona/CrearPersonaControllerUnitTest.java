package ar.edu.undec.adapter.servicetest.persona;

import ar.edu.undec.adapter.service.persona.controller.CrearPersonaController;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import persona.exception.PersonaYaExisteException;
import persona.input.CrearPersonaInput;
import persona.usecase.crearpersonausecase.CrearPersonaRequestModel;

import java.time.LocalDate;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CrearPersonaControllerUnitTest {

    @InjectMocks
    CrearPersonaController crearPersonaController;

    @Mock
    CrearPersonaInput crearPersonaInput;

    @Test
    public void crearPersona_PersonaCreada_RetornaHTTP201() {
        //Arrange
        when(crearPersonaInput.crearPersona(any(CrearPersonaRequestModel.class))).thenReturn(1);
        ResponseEntity<?> crearPersonaResponse = crearPersonaController.crearPersona(CrearPersonaRequestModel.instancia("123",
                "Juan", "fakeEmail", "123", LocalDate.now(), 1));
        Assertions.assertEquals(HttpStatus.CREATED, crearPersonaResponse.getStatusCode());
        Assertions.assertEquals(1, crearPersonaResponse.getBody());

    }

    @Test
    public void crearPersona_PersonaExisteException_RetornaHTTP400() {
        //Arrange
        doThrow(PersonaYaExisteException.class).when(crearPersonaInput).crearPersona(any(CrearPersonaRequestModel.class));
        ResponseEntity<?> crearPersonaResponse = crearPersonaController.crearPersona(CrearPersonaRequestModel.instancia("123",
                "Juan", "fakeEmail", "123", LocalDate.now(), 1));
        Assertions.assertEquals(HttpStatus.BAD_REQUEST, crearPersonaResponse.getStatusCode());
    }

}

