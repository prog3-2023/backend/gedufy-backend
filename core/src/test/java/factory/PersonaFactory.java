package factory;

import persona.model.Persona;
import persona.usecase.crearpersonausecase.CrearPersonaRequestModel;

import java.time.LocalDate;

public class PersonaFactory {
    private PersonaFactory() {
    }

    public static Persona factoryPersona(Integer id) {
        return Persona.instancia(id, "27444555", "Manu Ginobili", "manu@mail.com", "+543516112233", LocalDate.of(1980, 1, 20), TipoPersonaFactory.factoryTipoPersona(1));
    }

    public static CrearPersonaRequestModel factoryCrearPersonaRequestModel(Integer tipoPersonaId) {
        CrearPersonaRequestModel crearPersonaRequestModel = CrearPersonaRequestModel.instancia("27444555", "Manu Ginobili", "manu@mail.com", "+543516112233", LocalDate.of(1980, 1, 20), tipoPersonaId);
        return crearPersonaRequestModel;
    }
}
