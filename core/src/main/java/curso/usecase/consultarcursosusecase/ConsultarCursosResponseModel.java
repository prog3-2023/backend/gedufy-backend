package curso.usecase.consultarcursosusecase;

import curso.exception.CursoMapeoResponseException;

public class ConsultarCursosResponseModel {
    private Integer id;
    private String nombre;
    private String url;
    private String imagen;
    private Integer cantHoras;
    private Double precio;
    private String categoria;

    private ConsultarCursosResponseModel() {
    }

    private ConsultarCursosResponseModel(Integer id, String nombre, String url, String imagen, Integer cantHoras, Double precio, String categoria) {
        this.id = id;
        this.nombre = nombre;
        this.url = url;
        this.imagen = imagen;
        this.cantHoras = cantHoras;
        this.precio = precio;
        this.categoria = categoria;
    }

    public static ConsultarCursosResponseModel instancia(Integer id, String nombre, String url, String imagen, Integer cantHoras, Double precio, String categoria) {
        try {
            ConsultarCursosResponseModel consultarCursosResponseModel = new ConsultarCursosResponseModel(
                    id,
                    nombre,
                    url,
                    imagen,
                    cantHoras,
                    precio,
                    categoria);
            return consultarCursosResponseModel;
        } catch (Exception e) {
            throw new CursoMapeoResponseException("Error al crear instancia de ConsultarCursosResponseModel");
        }
    }

    public Integer getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public String getUrl() {
        return url;
    }

    public String getImagen() {
        return imagen;
    }

    public Integer getCantHoras() {
        return cantHoras;
    }

    public Double getPrecio() {
        return precio;
    }

    public String getCategoria() {
        return categoria;
    }
}
