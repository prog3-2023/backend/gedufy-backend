package persona.output;

import persona.model.Persona;

public interface CrearPersonaRepository {
    boolean existePorDNI(String dni);

    boolean existePorEmail(String email);

    Integer guardarPersona(Persona persona);
}
