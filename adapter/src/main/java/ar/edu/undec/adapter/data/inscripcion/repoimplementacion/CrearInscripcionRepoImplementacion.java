package ar.edu.undec.adapter.data.inscripcion.repoimplementacion;

import ar.edu.undec.adapter.data.inscripcion.crud.CrearInscripcionCRUD;
import ar.edu.undec.adapter.data.inscripcion.mapper.InscripcionDataMapper;
import inscripcion.model.Inscripcion;
import inscripcion.output.CrearInscripcionRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class CrearInscripcionRepoImplementacion implements CrearInscripcionRepository {

    private CrearInscripcionCRUD crearInscripcionCRUD;

    @Autowired
    public CrearInscripcionRepoImplementacion(CrearInscripcionCRUD crearInscripcionCRUD) {
        this.crearInscripcionCRUD = crearInscripcionCRUD;
    }

    @Override
    public Integer guardarInscripcion(Inscripcion inscripcion) {
        return crearInscripcionCRUD.save(InscripcionDataMapper.dataEntityMapper(inscripcion)).getId();
    }
}
