package usecase;

import curso.input.CrearCursoInput;
import curso.model.Curso;
import curso.output.CrearCursoRepository;
import curso.usecase.CrearCursoUseCase;
import factory.CursoFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CrearCursoUseCaseUnitTest {
    CrearCursoInput crearCursoInput;
    @Mock
    CrearCursoRepository crearCursoRepository;

    @BeforeEach
    void setup() {
        crearCursoInput = new CrearCursoUseCase(crearCursoRepository);
    }

    @Test
    void crearCurso_CursoNuevo_CreaCurso() {
        Curso curso = CursoFactory.factoryCurso(null);
        when(crearCursoRepository.guardarCurso(curso)).thenReturn(1);
        Assertions.assertEquals(1, crearCursoInput.crearCurso(curso));
    }
}
