package factory;

import tipopersona.model.TipoPersona;

public class TipoPersonaFactory {
    private TipoPersonaFactory() {
    }

    public static TipoPersona factoryTipoPersona(Integer id) {
        return TipoPersona.instancia(id, "Alumno", "descripcion");
    }
}
