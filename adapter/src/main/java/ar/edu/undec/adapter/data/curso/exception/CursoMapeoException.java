package ar.edu.undec.adapter.data.curso.exception;

public class CursoMapeoException extends RuntimeException {
    public CursoMapeoException(String message) {
        super(message);
    }
}
