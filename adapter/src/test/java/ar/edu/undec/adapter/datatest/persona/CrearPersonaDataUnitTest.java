package ar.edu.undec.adapter.datatest.persona;

import ar.edu.undec.adapter.data.persona.crud.CrearPersonaCRUD;
import ar.edu.undec.adapter.data.persona.model.PersonaEntity;
import ar.edu.undec.adapter.data.persona.repoimplementacion.CrearPersonaRepoImplementacion;
import ar.edu.undec.adapter.data.tipopersona.model.TipoPersonaEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import persona.model.Persona;
import tipopersona.model.TipoPersona;

import java.time.LocalDate;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CrearPersonaDataUnitTest {

    @InjectMocks
    CrearPersonaRepoImplementacion crearPersonaRepoImplementacion;

    @Mock
    CrearPersonaCRUD crearPersonaCRUD;

    @Test
    public void guardarPersona_PersonaGuardada_DevuelveNuevoID(){
        when(crearPersonaCRUD.save(any(PersonaEntity.class))).thenReturn(new PersonaEntity(1,
                "123",
                "Juan",
                "mail@mail.com",
                "123",
                LocalDate.now(),
                new TipoPersonaEntity(1,"tipoPersona", "descripcion")));
        Integer resultado=crearPersonaRepoImplementacion.guardarPersona(Persona.instancia(null,
                "123",
                "Juan",
                "mail@mail.com",
                "123",
                LocalDate.now(),
                TipoPersona.instancia(1,"tipoPersona", "descripcion")));
        Assertions.assertEquals(1,resultado);
    }

    @Test
    public void guardarPersona_DatabaseException_Devuelve0(){
        doThrow(RuntimeException.class).when(crearPersonaCRUD).save(any(PersonaEntity.class));
        Integer resultado=crearPersonaRepoImplementacion.guardarPersona(Persona.instancia(null,
                "123",
                "Juan",
                "mail@mail.com",
                "123",
                LocalDate.now(),
                TipoPersona.instancia(1,"tipoPersona", "descripcion")));
        Assertions.assertEquals(0,resultado);
    }
}
