package ar.edu.undec.adapter.data.inscripcion.exception;

public class InscripcionMapeoException extends RuntimeException {
    public InscripcionMapeoException(String message) {
        super(message);
    }
}
