package persona.usecase.crearpersonausecase;

import java.time.LocalDate;

public class CrearPersonaRequestModel {

    private String dni;
    private String nombre;
    private String email;
    private String telefono;
    private LocalDate fecha_nacimiento;
    private Integer tipo_persona_id;

    private CrearPersonaRequestModel() {
    }

    private CrearPersonaRequestModel(String dni, String nombre, String email, String telefono, LocalDate fecha_nacimiento, Integer tipo_persona_id) {
        this.dni = dni;
        this.nombre = nombre;
        this.email = email;
        this.telefono = telefono;
        this.fecha_nacimiento = fecha_nacimiento;
        this.tipo_persona_id = tipo_persona_id;
    }

    public static CrearPersonaRequestModel instancia(String dni, String nombre, String email, String telefono, LocalDate fecha_nacimiento, Integer tipo_persona_id) {
        try {
            CrearPersonaRequestModel crearPersonaRequestModel = new CrearPersonaRequestModel(
                    dni,
                    nombre,
                    email,
                    telefono,
                    fecha_nacimiento,
                    tipo_persona_id);
            return crearPersonaRequestModel;
        } catch (Exception e) {
            throw new RuntimeException("Error al crear instancia de CrearPersonaRequestModel");
        }
    }

    public String getDni() {
        return dni;
    }

    public String getNombre() {
        return nombre;
    }

    public String getEmail() {
        return email;
    }

    public String getTelefono() {
        return telefono;
    }

    public LocalDate getFecha_nacimiento() {
        return fecha_nacimiento;
    }

    public Integer getTipo_persona_id() {
        return tipo_persona_id;
    }
}
